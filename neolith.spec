Summary:	The Neolith LPmud server
Name:		neolith
Version:	0.1.2
Release:	2
Copyright:	LPmud + MudOS + GPL
Group:		Applications/Internet
Packager:	Annihilator <annihilator@muds.net>
URL:		http://www.es2.muds.net/neolith
Source:		neolith-0.1.2.tar.gz
Prefix:		%{_prefix}
BuildRoot:	%{_tmppath}

%description
The Neolith LPmud server is a text-based internet game server which enables
its users to talk, find treasures, kill monsters (or each other) in a virtual
reality constructed using its builtin programming language.

If you are looking for a LPmud driver, this should worth a try.

#############################################################################

%prep
%setup

%build
%configure
make

%install
rm -rf ${RPM_BUILD_ROOT}%{_prefix}
%makeinstall LDFLAGS=-s prefix=${RPM_BUILD_ROOT}%{_prefix}

%post
ldconfig -n %{_libdir}/neolith

%clean
rm -rf ${RPM_BUILD_ROOT}%{_prefix}

%files
%defattr(-,root,root)
%doc README NEWS INSTALL AUTHORS COPYING doc
%{_bindir}/*
%{_datadir}/locale/*/LC_MESSAGES/neolith.mo
%{_libdir}/neolith/*.so*
%dir %{_libdir}/neolith

#############################################################################
%package devel

Summary: The development library for Neolith LPmud server
Group: Development/Libraries
Requires: neolith >= 0.1.2

%description devel
Install this package if you are writing modules with Neolith LPmud server.

%files devel
%defattr(-,root,root)
#%{_includedir}/neolith/*.h
#%dir %{_includedir}/neolith
%{_libdir}/neolith/*.la
