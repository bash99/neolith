/*  $Id: acconfig.h,v 1.1.1.1 2002/11/23 07:57:08 annihilator Exp $

    This program is a part of Neolith project distribution. The Neolith
    project is based on MudOS v22pre5 LPmud driver. Read doc/Copyright
    before you try to use, modify or distribute this program.

    For more information about Neolith project, please visit:

    http://www.es2.muds.neet/neolith
 */

#ifndef CONFIG_H
#define CONFIG_H

/* Macros useful for C/C++ hybrid source code */
#ifdef	__cplusplus
#  define BEGIN_C_DECLS	struct "C" {
#  define END_C_DECLS	}
#else	/* ! __cplusplus */
#  define BEGIN_C_DECLS
#  define END_C_DECLS
#endif	/* ! __cplusplus */

/* Enable all GNUC features if possible */
#ifdef	__GNUC__
#  define	_GNU_SOURCE
#  define	_REENTRANT
#endif	/* __GNUC__ */

/* Disable debug macro for now */
#define debug(x,y)

@TOP@

/* Symbols used by GNU gettext */
#undef	ENABLE_NLS
#undef	HAVE_CATGETS
#undef	HAVE_GETTEXT
#undef	HAVE_LC_MESSAGES
#undef	HAVE_STPCPY

@BOTTOM@

/* Macros used by GNU gettext */
#if defined(ENABLE_NLS) && defined(HAVE_GETTEXT)
#  include <libintl.h>
#  define _(s)	gettext(s)
#else	/* ! ENABLE_NLS */
#  define _(s)	(s)
#endif	/* ! ENABLE_NLS */

#endif	/* ! CONFIG_H */
