/*  $Id: hash.h,v 1.1.1.1 2002/11/23 07:57:08 annihilator Exp $

    This program is a part of Neolith project distribution. The Neolith
    project is based on MudOS v22pre5 LPmud driver. Read doc/Copyright
    before you try to use, modify or distribute this program.

    For more information about Neolith project, please visit:

    http://www.es2.muds.net/neolith
 */

#ifndef	ADT_HASH_H
#define	ADT_HASH_H

BEGIN_C_DECLS

inline int hashstr(char *, int, int);
inline int whashstr(char *, int);

END_C_DECLS

#endif	/* ! ADT_HASH_H */
