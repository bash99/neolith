#include <iconv.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "config.h"

#include "src/std.h"
#include "_lpc/object.h"
#include "_lpc/array.h"
#include "_lpc/mapping.h"
#include "_lpc/program.h"
#include "src/simulate.h"
#include "src/interpret.h"
#include "src/stralloc.h"
#include "LPC/function.h"
#include "src/applies.h"
#include "src/simul_efun.h"
#include "src/lex.h"

#include "gb2utf.h"

char out[32768];


void
f_gb2utf8 (void)
{
    int len;
    char *buf;
    char *out1;
    int outlen, i;

    if (sp->type == T_STRING) {
        len = SVALUE_STRLEN(sp);
        buf = (unsigned char *) sp->u.string;
        /*
#ifndef NO_BUFFER_TYPE
    } else if (sp->type == T_BUFFER) {
        len = sp->u.buf->size;
        buf = sp->u.buf->item;
#endif
        */
    }
    
    out[0] = 0;
	if(len ==0) {
/*    		fprintf(stderr, "pass null string to gb2utf8, input len: %d, input: %s\n", len, buf);
		debug_message("pass null string to gb2utf8\n");
        	if (current_file)
	            debug_message("(occured during compilation of %s at line %d)\n", current_file, current_line);
	        if (current_object)
	            debug_message("(current object was /%s)\n", current_object->name);
*/
	        dump_trace(1);
		return; // buf overflow
	}
	if((len*3+3) > 32768) return;
		
    computing_gb2utf8(buf,len,out,&outlen,len*3+3);
    if (outlen == 0) {
    	fprintf(stderr, "Nothing converted in gb2utf8, input len: %d, input: %s, outlen: %d\n", len, buf, outlen);
    }
   	out1 = new_string(outlen, "f_gb2utf8");
   	for(i=0; i<outlen; i++)
   		out1[i] = out[i];
   	out1[outlen] = 0;
    free_string_svalue(sp);
    put_malloced_string(out1);
    return;
}

char *computing_gb2utf8 (char *input, int ilen, char *output, int *olen, int outmax)
{
	char *inptr, *outptr;
	size_t nc;
	static iconv_t cd;
	static int start = 0;
	int smax;
	smax = outmax;
	inptr = input; outptr = output;
	if(!start) {
		cd = iconv_open("utf8","gbk");
		fprintf(stderr, "gb2utf8 init iconv\n");
		start = 1;
	}
    iconv(cd, 0, 0, 0, 0);
	nc = iconv(cd,&inptr,&ilen,&outptr,&outmax);
	if (nc == -1) {
		*olen = 0;
		perror("something is wrong in iconv gb2utf8 \n");
	}
	else *olen = smax-outmax;
	//iconv_close(cd);
	return output;
}

