/*  $Id: operator.h,v 1.1.1.1 2002/11/23 07:57:08 annihilator Exp $

    This program is a part of Neolith project distribution. The Neolith
    project is based on MudOS v22pre5 LPmud driver. Read doc/Copyright
    before you try to use, modify or distribute this program.

    For more information about Neolith project, please visit:

    http://www.es2.muds.net/neolith
 */

#ifndef	EFUNS_OPERATOR_H
#define	EFUNS_OPERATOR_H

BEGIN_C_DECLS

inline void f_ge(void);
inline void f_le(void);
inline void f_lt(void);
inline void f_gt(void);
inline void f_and(void);
inline void f_and_eq(void);
inline void f_div_eq(void);
inline void f_eq(void);
inline void f_lsh(void);
inline void f_lsh_eq(void);
inline void f_mod_eq(void);
inline void f_mult_eq(void);
inline void f_ne(void);
inline void f_or(void);
inline void f_or_eq(void);
inline void f_parse_command(void);
inline void f_range(int);
inline void f_extract_range(int);
inline void f_rsh(void);
inline void f_rsh_eq(void);
inline void f_simul_efun(void);
inline void f_sub_eq(void);
inline void f_switch(void);
inline void f_xor(void);
inline void f_xor_eq(void);
inline void f_function_constructor(void);
inline void f_evaluate(void);
inline void f_sscanf(void);

/*
 * eoperators.c
 */
inline funptr_t *make_funp(svalue_t *, svalue_t *);
inline void push_funp(funptr_t *);
inline void free_funp(funptr_t *);
int merge_arg_lists(int, array_t *, int);
void call_simul_efun(unsigned short, int);

inline funptr_t *make_efun_funp(int, svalue_t *);
inline funptr_t *make_lfun_funp(int, svalue_t *);
inline funptr_t *make_simul_funp(int, svalue_t *);

END_C_DECLS

#endif	/* ! EFUNS_OPERATOR_H */
