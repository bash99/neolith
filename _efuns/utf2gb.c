#include <iconv.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

#include "src/std.h"
#include "_lpc/object.h"
#include "_lpc/array.h"
#include "_lpc/mapping.h"
#include "_lpc/program.h"
#include "src/simulate.h"
#include "src/interpret.h"
#include "src/stralloc.h"
#include "LPC/function.h"
#include "src/applies.h"
#include "src/simul_efun.h"
#include "src/lex.h"

#include "utf2gb.h"

char out[32768];

void
f_utf82gb (void)
{
    int len;
    char *buf;
    char *out1;
    int outlen, i = 0;

    if (sp->type == T_STRING) {
        len = SVALUE_STRLEN(sp);
        buf = (unsigned char *) sp->u.string;
    }
    /* else if (sp->type == T_BUFFER) {
      len = sp->u.buf->size;
      buf = sp->u.buf->item;
      }*/
    
	if(len == 0) {
		fprintf(stderr, "warning, null input str, input len: %d, input: %s\n", len, buf);
		return; // buf overflow
	} 
	if( (len+3) > 32768) return;
    out[0] = 0;
    computing_utf82gb(buf,len,out,&outlen,len+3);
    if (outlen == -1) {
    	fprintf(stderr, "Iconv error in utf82gb.\n");
    	return;
    }
    else { 
		if (outlen == 0) {
    		fprintf(stderr, "Nothing converted in utf82gb, input len: %d, input: %s, outlen: %d\n", len, buf, outlen);
    	}
	   	out1 = new_string(outlen, "f_utf82gb");
	   	for(i=0; i<outlen; i++)
    		out1[i] = out[i];
    	out1[outlen] = 0;
	    free_string_svalue(sp);
        put_malloced_string(out1);
	}
}


char *computing_utf82gb (char *input, int ilen, char *output, int *olen, int outmax)
{
	char *inptr, *outptr;
	size_t nc;
	static iconv_t cd;
	static int start = 0;
	int smax;
	smax = outmax;
	inptr = input; outptr = output;
	if(!start) {
		cd = iconv_open("gbk","utf8");
		fprintf(stderr, "utf82gb init iconv\n");
		start = 1;
	}
	iconv(cd, 0, 0, 0, 0);
	nc = iconv(cd,&inptr,&ilen,&outptr,&outmax);
	if (nc == -1) {
		*olen = 0;
		perror("something is wrong in iconv utf82gb \n");
	}
	else *olen = smax-outmax;
	//iconv_close(cd);
	return output;
}
